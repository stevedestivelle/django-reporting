__author__ = 'Administrator'
#!/usr/bin/python
# -*- coding: utf-8 -*-
import StringIO
import xlsxwriter
from django.utils.translation import ugettext
from django.db.models import Avg, Sum, Max, Min

from models import OrderTab


def WriteToExcel(objects, town=None):
    output = StringIO.StringIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet_s = workbook.add_worksheet("Summary")

    # excel styles
    title = workbook.add_format({
        'bold': True,
        'font_size': 14,
        'align': 'center',
        'valign': 'vcenter'
    })
    header = workbook.add_format({
        'bg_color': '#F7F7F7',
        'color': 'black',
        'align': 'center',
        'valign': 'top',
        'border': 1
    })
    cell = workbook.add_format({
        'align': 'left',
        'valign': 'top',
        'text_wrap': True,
        'border': 1
    })
    cell_center = workbook.add_format({
        'align': 'center',
        'valign': 'top',
        'border': 1
    })

    # write title
    order_text = ugettext("all recorded orders")
    title_text = u"{0} {1}".format(ugettext("Weather History for"), order_text)
    # merge cells
    worksheet_s.merge_range('B2:I2', title_text, title)

    # write header
    worksheet_s.write(4, 0, ugettext("No"), header)
    worksheet_s.write(4, 1, ugettext("OrderId"), header)
    worksheet_s.write(4, 2, ugettext("UID"), header)
    worksheet_s.write(4, 3, ugettext("Create time"), header)
    worksheet_s.write(4, 4, ugettext("Update time"), header)

    # column widths
    town_col_width = 10
    description_col_width = 10
    observations_col_width = 25

    # add data to the table
    for idx, data in enumerate(objects):
        row = 5 + idx
        worksheet_s.write_number(row, 0, idx + 1, cell_center)
        worksheet_s.write_string(row, 1, data.order_id, cell)
        worksheet_s.write(row, 2, data.uid, cell_center)
        worksheet_s.write_string(row, 3, data.create_time.strftime('%d/%m/%Y'), cell)
        worksheet_s.write_number(row, 4, data.update_time.strftime('%d/%m/%Y'), cell_center)

    # change column widths
    worksheet_s.set_column('B:B', town_col_width)  # Town column
    worksheet_s.set_column('C:C', 11)  # Date column
    worksheet_s.set_column('D:D', description_col_width)  # Description column
    worksheet_s.set_column('E:E', 10)  # Max Temp column
    worksheet_s.set_column('F:F', 10)  # Min Temp column

    row = row + 1

    # close workbook
    workbook.close()
    xlsx_data = output.getvalue()
    return xlsx_data