__author__ = 'Administrator'
from django.conf.urls import url
from . import views
app_name = 'reporting'
urlpatterns = [
     url(r'^excel-report$', views.excel_export, name='excel_export'),
    url(r'^$', views.index, name='index'),
    url(r'^tables$', views.tables, name='tables'),
    url(r'^table-dynamic$', views.table_dynamic, name='table_dynamic'),
]