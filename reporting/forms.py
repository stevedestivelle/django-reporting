from django import forms

class DateToDateForm(forms.Form):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'class':'form-control has-feedback-left datepicker', 'placeholder': 'Start date', 'aria-describedby': 'inputSuccess2Status'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'class':'form-control has-feedback-left datepicker', 'placeholder': 'End date', 'aria-describedby': 'inputSuccess2Status'}))
    orderId = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs={'class':'form-control has-feedback-left', 'placeholder': 'Order ID', 'aria-describedby': 'inputSuccess2Status'}))
    uid = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs={'class':'form-control has-feedback-left', 'placeholder': 'UID', 'aria-describedby': 'inputSuccess2Status'}))