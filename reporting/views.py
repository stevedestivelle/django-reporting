import urllib

__author__ = 'Administrator'

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpRequest
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.db.models import Q, F


from models import OrderTab
from sortheader import SortHeaders
from forms import DateToDateForm
from datetime import datetime,timedelta
from excel_utils import WriteToExcel

import time
import constants


LIST_HEADERS = (
    ('OrderId', 'order_id'),
    ('UID', 'uid'),
    ('Create time', 'create_time'),
    ('Update time', 'update_time'),
)

DEFAULT_ORDER = (0)
DEFAULT_ORDER_TYPE = 'desc'

def index(request):
    return render(request, 'reporting/base_index.html', {'hello': 'xin chao chaulaode'})

def tables(request):
    return render(request, 'reporting/base_tables.html', {'variable': 'chaulaode'})

@login_required(redirect_field_name='/reporting/table_dynamic/',login_url='/admin/')
@permission_required('reporting', raise_exception=True)
def table_dynamic(request):

    form = DateToDateForm(request.GET)
    start_date = request.GET.get('start_date', '')
    end_date = request.GET.get('end_date', '')

    if not start_date:
        DAY = timedelta(constants.START_DATE)
        start_date = datetime.today() - DAY

    else:
         start_date = datetime.strptime(start_date, '%Y-%m-%d')- timedelta(days=1)
    if not end_date:
        end_date = datetime.now().date()
        form = DateToDateForm(initial={'start_date':start_date, 'end_date':end_date})
    else:
        end_date = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)

    uid = request.GET.get('uid', '')
    orderId = request.GET.get('orderId', '')


    start_date = int(time.mktime(start_date.timetuple()))
    end_date = int(time.mktime(end_date.timetuple()))

    sort_headers = SortHeaders(request, LIST_HEADERS, DEFAULT_ORDER, DEFAULT_ORDER_TYPE)
    objects = OrderTab.objects.filter(
        create_time__gt = start_date,
        create_time__lt = end_date,
        order_id__contains=orderId,
        uid__contains=uid
    ).order_by(sort_headers.get_order_by())

    if 'excel' in request.GET:
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=Report.xlsx'
        xlsx_data = WriteToExcel(objects)
        response.write(xlsx_data)
        return response

    paginator = Paginator(objects, constants.NUM_PER_PAGE)
    page = request.GET.get('page')

    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        objects = paginator.page(paginator.num_pages)

    return render(
        request,
        'reporting/base_table_dynamic.html',
        {'order_list': objects, 'headers': list(sort_headers.headers()),'url':request.get_full_path(), 'form': form}
    )

def excel_export(request):
    import xlsxwriter
    import StringIO
    output = StringIO.StringIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()
    expenses = (
        ['Rent', 1000],
        ['Gas', 100],
        ['Food', 300],
        ['Gym', 50]
    )
    row = 0
    col = 0

    for item, cost in (expenses):
        worksheet.write(row, col, item)
        worksheet.write(row, col+1, cost)
        row+=1

    workbook.close()

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=Report.xlsx'
    response.write('Report.xlsx')
    return response