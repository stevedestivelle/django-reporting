from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
app_name = 'airpay'
urlpatterns = [
    url(r'^total_user/$', 'airpay.views.total_user'),
    url(r'^new_user/$', 'airpay.views.new_user'),
    url(r'^active_user/$', 'airpay.views.active_user'),
    url(r'^transaction_user/$', 'airpay.views.transaction_user'),
    url(r'^churn_user/$', 'airpay.views.churn_user'),
    url(r'^new_card/$', 'airpay.views.new_card'),
    url(r'^total_card/$', 'airpay.views.total_card'),
    url(r'^bank_account/$', 'airpay.views.bank_account'),
    url(r'^credit_card/$', 'airpay.views.credit_card'),

    url(r'^transaction/$', 'airpay.views.transaction'),
    url(r'^transaction_by_payment/$', 'airpay.views.transaction_by_payment'),
    url(r'^shopee_order/$', 'airpay.views.shopee_order'),
    url(r'^version/$', 'airpay.views.version'),
    url(r'^version_active/$', 'airpay.views.version_active'),

    url(r'^home_screen/$', 'airpay.views.home_screen'),
    url(r'^home_banner/$', 'airpay.views.home_banner'),
    url(r'^purchase_flow/$', 'airpay.views.purchase_flow'),
    url(r'^gift/$', 'airpay.views.gift'),
    url(r'^ewallet/$', 'airpay.views.ewallet'),
    url(r'^gift_tracking/$', 'airpay.views.gift_tracking'),
    url(r'^general/$', 'airpay.views.general'),
]